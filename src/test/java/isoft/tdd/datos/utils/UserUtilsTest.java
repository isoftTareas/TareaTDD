package isoft.tdd.datos.utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserUtilsTest {

    UserUtils userUtils;

    @BeforeEach
    public void setUp(){
        userUtils = new UserUtils();
    }
    @Test
    @DisplayName("Test validar nombre")
    public void validarNombreTest(){
        assertTrue(userUtils.validarNombre("Juan"));
    }

    @Test
    @DisplayName("Test validar largo rut")
    public void validarLargoRutTest(){
        assertTrue(userUtils.validarLargoRut("12345678-9"));
    }
    @Test
    @DisplayName("Test validar digito verificador")
    public void validarDigitoVerificadorTest(){
        assertTrue(userUtils.digVerificadorRut("20102523-0"));
    }
    @Test
    @DisplayName("Test validar largo telefono")
    public void validarLargoTelefonoTest(){
        assertTrue(userUtils.validarLargoTelefono("12345678"));
    }
    @Test
    @DisplayName("Test validar telefono sin letras")
    public void validarTelefonoSinLetrasTest(){
        assertTrue(userUtils.validarTelefonoSinLetras("12345678"));
    }
    @Test
    @DisplayName("Test validar largo edad")
    public void validarLargoEdadTest(){
        assertTrue(userUtils.validarLargoEdad("12345678"));
    }
    @Test
    @DisplayName("Test validar valor edad")
    public void validarValorEdadTest(){
        assertTrue(userUtils.validarValorEdad("12345678"));
    }

}
